//Profil.js
if (authentifier()) {
    window.addEventListener('DOMContentLoaded', init, false);

    function init() {
        infoJoueur();
        document.getElementById("formUpdate").addEventListener("submit", putDB, false);
    }


    function infoJoueur() {
        getIdUser(function(result) {
            var req = $.ajax({
                type: "GET",
                url: 'http://localhost:3000/users/' + result,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({})
            });

            req.done(function(result) {
                document.getElementById('name').value = result.name;
            });


        });
    }

    function putDB(e) {
        var dataJson;

        getIdUser(function(result) {
            var req = $.ajax({
                type: "PUT",
                url: 'http://localhost:3000/USERS/UPDATE/' + result,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    "name": document.getElementById('name').value,
                    "password": document.getElementById('password').value
                })
            });
            console.log(req);

            req.done(function(result) {

                document.location.href = "/"

            });

            req.fail(function(err) {
                document.getElementById('danger').textContent = err.responseText;
            });

        });

        e.preventDefault();


    }


} else {
    document.location.href = "/users/login";
}